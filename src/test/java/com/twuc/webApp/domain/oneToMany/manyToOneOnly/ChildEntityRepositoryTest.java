package com.twuc.webApp.domain.oneToMany.manyToOneOnly;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class ChildEntityRepositoryTest extends JpaTestBase {

    @Autowired
    private ChildEntityRepository childEntityRepository;

    @Autowired
    private ParentEntityRepository parentEntityRepository;

    @Test
    void should_save_parent_and_child_entity() {
        // TODO
        //
        // 请书写测试存储一对 one-to-many 的 child-parent 关系。并从 child 方面进行查询来证实存储已经
        // 成功。
        //
        // <--start-
        ParentEntity parent = new ParentEntity("parent");
        ChildEntity child = new ChildEntity("child");
        child.setParentEntity(parent);
        ClosureValue<Long> childId = new ClosureValue<>();

        flushAndClear(entityManager -> {
            ParentEntity parentEntity = parentEntityRepository.save(parent);
            ChildEntity childEntity = childEntityRepository.save(child);
            childId.setValue(childEntity.getId());
        });

        run(entityManager -> {
            ChildEntity childEntity = childEntityRepository.findById(childId.getValue()).get();
            assertEquals("child", childEntity.getName());
            assertEquals("parent", childEntity.getParentEntity().getName());
        });
         // --end-->
    }

    @Test
    void should_remove_the_child_parent_relationship() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 解除 child 和 parent 的关系
        // Then child 和 parent 仍然存在，但是 child 不再引用 parent。
        //
        // <--start-
        ParentEntity parent = new ParentEntity("parent");
        ChildEntity child = new ChildEntity("child");
        child.setParentEntity(parent);
        ClosureValue<Long> childId = new ClosureValue<>();

        flushAndClear(entityManager -> {
            ParentEntity parentEntity = parentEntityRepository.save(parent);
            ChildEntity childEntity = childEntityRepository.save(child);
            childId.setValue(childEntity.getId());
        });

        flushAndClear(entityManager -> {
            ChildEntity childEntity = childEntityRepository.findById(childId.getValue()).get();
            childEntity.setParentEntity(null);
        });

        run(entityManager -> {
            ChildEntity childEntity = childEntityRepository.findById(childId.getValue()).get();
            assertEquals("child", childEntity.getName());
            assertEquals("parent", parentEntityRepository.findAll().get(0).getName());
            assertNull(childEntity.getParentEntity());
        });
        // --end-->
    }

    @Test
    void should_remove_child_and_parent() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 删除 child 和 parent。
        // Then child 和 parent 不再存在。
        //
        // <--start-
        ParentEntity parent = new ParentEntity("parent");
        ChildEntity child = new ChildEntity("child");
        child.setParentEntity(parent);
        ClosureValue<Long> childId = new ClosureValue<>();
        ClosureValue<Long> parentId = new ClosureValue<>();

        flushAndClear(entityManager -> {
            ParentEntity parentEntity = parentEntityRepository.save(parent);
            ChildEntity childEntity = childEntityRepository.save(child);
            childId.setValue(childEntity.getId());
            parentId.setValue(parentEntity.getId());
        });

        flush(entityManager -> {
            childEntityRepository.deleteById(childId.getValue());
            parentEntityRepository.deleteById(parentId.getValue());
        });

        run(entityManager -> {
            assertEquals(0, parentEntityRepository.findAll().size());
            assertEquals(0, childEntityRepository.findAll().size());
        });
        // --end-->
    }
}